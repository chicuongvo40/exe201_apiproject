﻿using AccessData.Models;
using AccessData.Repository;
using Microsoft.AspNetCore.Mvc;
using Project.Data.Models;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CloudFoneController : Controller
    {
        private ICloudFoneRepo repository = new CloudFoneRepo();
        [HttpPost]
        public IActionResult PostMember([FromBody] CloudFoneEntityDTO memberRequest)
        {
            var _phone = new CloudFone
            {
                ApiKey = memberRequest.ApiKey,
                CallNumber = memberRequest.CallNumber,
                CallName = memberRequest.CallName,
                QueueNumber = memberRequest.QueueNumber,
                ReceiptNumber = memberRequest.ReceiptNumber,
                Key = memberRequest.Key,
                KeyRinging = memberRequest.KeyRinging,
                Status = memberRequest.Status,
                Direction = memberRequest.Direction,
                NumberPbx = memberRequest.NumberPbx,
                Message = memberRequest.Message,
                Data = "",
            };
            repository.Save(_phone);
            return Ok(memberRequest);
        }
    }
}
