﻿using AccessData.Models;
using Project.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessData.Repository
{
    public interface ICloudFoneRepo
    {
        void Save(CloudFone phone);
    }
}
