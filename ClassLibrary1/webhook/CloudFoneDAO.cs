﻿using AccessData.Models;
using Project.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessData
{
    public class CloudFoneDAO
    {
        public static void Save(CloudFone _user)
        {
            try
            {
                using (var context = new DemoCode3Context())
                {
                    context.CloudFones.Add(_user);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
